import random
import time

def bubble_sort(sortable):
    return []


def merge_sort(sortable):
    return []


def do_sort(method, sortable):
    start_time = time.time()
    sorted_list = method(sortable[:])
    elapsed_time = time.time() - start_time
    correct = sorted_list == sorted(sortable)
    print('{} executed in {} seconds {}'.format(method.__name__, elapsed_time, 'correctly' if correct else 'incorrectly'))


def main():
    original = [random.random() for i in range(2000)]
    do_sort(bubble_sort, original)
    do_sort(merge_sort, original)


if __name__ == '__main__':
    main()
